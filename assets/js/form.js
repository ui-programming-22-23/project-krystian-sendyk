var canvas2 = document.getElementById("canvas");
canvas2.style.display ="none";

function clearStorage(){
    window.localStorage.clear();
}

if (localStorage.getItem('time') > 0)
{
    var time_current = localStorage.getItem('time');
} 
else
{
    var time_current = 100;
}
let time_max = 100;
let time_width = time_max;
let time_height = 30;
let time_x = 5;
let time_y = 640;

levelCount = localStorage.getItem('level'); //sets the scoreCount to last score stored unless game restarts
if (levelCount != "undefined"){
    
}
else{
    levelCount = 1;
}
var button = document.getElementById("button");
var button2 = document.getElementById("button2");


if(typeof(Storage) !== "undefined") 
{
    // console.log("Local storage is supported.");
    // Local storage is available on your browser
    const username = localStorage.getItem('username');//holds the user's name
    const level = localStorage.getItem('level'); //holds the user's level
    const time = localStorage.getItem('time'); //holds the user's health
    if (username)
    {
        let form = document.forms["playerName"];
        form.style.display = "none";
        let modal = document.getElementById("modal");
        let modalContent = modal.children[0].children[2];
        modal.style.display = "block";
        modalContent.innerHTML = "Username: " + username + "<br>" + "Level: " + level + "<br>" + "Time: " + time; //displays the user's level & name from previous game
        let header = document.getElementById("main-header");
        let validateButton = document.getElementsByClassName("saved-data-accept")[0];
        let dismissButton = document.getElementsByClassName("saved-data-refusal")[0];
        button.style.display = "none";
        button2.style.display = "none";

        validateButton.onclick = function() //starts the game
        {
            button.style.display = "block";
            button2.style.display = "block";
            form.style.display="none";
            modal.style.display = "none";
            canvas2.style.display = "initial";
        }
        dismissButton.onclick = function() //gets rid of all the previous score, name & health
        {
            button.style.display = "none";
            button2.style.display = "none";
            form.style.display = "block";
            modal.style.display="none";
            //the following is not necessary in this case, but I'll leave it here in case you need it later
            clearStorage();
        }
    }
    else
    {
        console.log("no data in localStorage, loading new session")
    }
} 
else 
{
    console.log("Local storage is not supported.");
    // The condition isn't met, meaning local storage isn't supported
}
// Stores the item data

function validateForm()
{
    event.preventDefault();
    var x = document.forms["playerName"]["name"].value;
    if (x == "") 
    {
        alert("I need to know your name so I can say Hello");
        return false;
    }
    else
    {
        alert("Hello there " + document.forms["playerName"]["name"].value);
        //more advanced pt2: make a system that changes the webpage based on the inputted name 
        canvas2.style.display = "initial";
        button.style.display = "block";
        button2.style.display = "block";

        playerName = document.getElementById("playerName");
        playerName.style.display="none";
    }
    localStorage.setItem("username", x);
    time_current = 100;
    levelCount = 1;
}