//Canvas
let canvas = document.getElementById("canvas");
let context = canvas.getContext("2d");

//items
var collected = false;
let collectedX = 50;
let collectedY = 50;
let showX = 1600;
let showY =50;
var countItem=0;
var collectedItem=0;

//player image
let playerSprite = new Image();
playerSprite.src = "assets/img/Green-16x18-spritesheet.png";
let player = new placeHolder(playerSprite, 850, 150, 100, 100);
let speed = 6; //speed of the player
let direction = 0;
const scale = 2;
const width = 16;
const height = 18;
const scaledWidth = scale * width;
const scaledHeight = scale * height;

//time limit
let count = 300;

//frames
let frameCount = 0;
const frameLast = 7;
const walkLoop =[0,1,0,2];
let currentLoop =0;

let countScore = 0;

//object image
let object1 = new Image();
object1.src = "assets/img/ball_red.png";
let object2 = new Image();
object2.src = "assets/img/ball_green.png";
let object3 = new Image();
object3.src = "assets/img/cap.png";
let object4 = new Image();
object4.src = "assets/img/crayonGreen.png";
//object image
let ball = new placeHolder(object1, 300, 300, 50, 48);
let ball2 = new placeHolder(object2, 700, 300, 50, 48);
let cap = new placeHolder(object3, 800, 200, 50, 48)
let crayonGreen = new placeHolder(object4,  200, 280, 50, 48)

let randomObject = new placeHolder(object1, showX, showY, 50, 48);

const itemArray = [ball, ball2, cap, crayonGreen];

//level
var levelCount = localStorage.getItem('level');
document.getElementById("level").innerHTML = "level: "+ localStorage.getItem('level');

//check if keyboard was pressed or let go
window.addEventListener('keydown', keyboardPressed); //catches if a key is pressed on the keyboard
window.addEventListener('keyup', keyboardPressed); //catches when the key pressed is let go

//remembers the last key/mouse pressed and passes onto keyboardpressed
function playerInput(keyboardPressed)
{
    this.action = keyboardPressed;
}

let input = new playerInput("none") //setting the current key/mouse pressed

//Joystick touch pad
var dynamic = nipplejs.create
({
    zone: document.getElementById('canvas'),
    color: 'blue',
});

//checks if key on keyboard is pressed
function keyboardPressed(event)
{
    //console.log(event);
    if(event.type === "keydown") //key is pressed this activates
    {
        switch(event.keyCode) //if the key pressed the box moves
        {
            case 37: //moves left
                input = new playerInput("left")
                //console.log("Left arrow");
                break;
            case 38://moves up
                input = new playerInput("up")
                //console.log("Up arrow");
                break;
            case 39://moves right
                input = new playerInput("right")
                //console.log("Right arrow");
                break;
            case 40://moves down
                input = new playerInput("down")
                //console.log("Down arrow");
                break;
            case 32://collect the necessary items
                if(collected == false)
                {collect();}
                else if(collected == true)
                {give();}
            default:
                input = new playerInput("none");
        }
    }
    else 
    {
        input = new playerInput("none");
    }
}

//holds the data that are required for player, enemy etc
function placeHolder(spritesheet, x, y, width, height)
{
    this.spritesheet = spritesheet; //image of the character
    this.x = x; // player's x position
    this.y = y; // player's y position
    this.width = width; // player's width position
    this.height = height; // player's height position
    this.directionMove = "none"; //the direction the player is moving
}

//detection if the player hits the edges of the border
function edgeDetection()
{
    //border check for left
    if(player.x <= canvas.width - canvas.width )
    {
        player.x = canvas.width - canvas.width;
    }
    //border check for top
    if(player.y <= canvas.height - canvas.height)
    {
        player.y = canvas.height - canvas.height;
    }
    //border check for right
    if(player.x >=canvas.width - player.width +60)
    {
        player.x = canvas.width - player.width +60;
    }
    //border check for bottom
    if(player.y >= canvas.height - player.height +60)
    {
        player.y = canvas.height - player.height +60;
    }
}

function playerInteracton()
 {
    document.getElementById("level").innerHTML = "level: "+ localStorage.getItem('level');
    localStorage.setItem("level", levelCount);
    document.getElementById("level").innerHTML = "level: "+ localStorage.getItem('level');
    localStorage.setItem("time", time_current); //stores the current time in localStorage

    if (input.action === "left")
    {
        player.x -= speed;
        direction = 2;
    }
    if(input.action === "right")
    {
        player.x += speed;
        direction = 3;
    }
    if(input.action === "up")
    {
        player.y -= speed;
        direction = 1;
    }
    if(input.action === "down")
    {
        player.y += speed;
        direction = 0;
    }
    if(input.action === "none")
    {}
}

function showItem()
{
    if(levelCount ==1 )
    {
        randomItem = Math.floor(Math.random() * 2);
        randomObject.spritesheet = itemArray[randomItem].spritesheet;
    }
    else if(levelCount ==2)
    {
        randomItem = Math.floor(Math.random() * 3);
        randomObject.spritesheet = itemArray[randomItem].spritesheet;
    }
    else if(levelCount ==3)
    {
        randomItem = Math.floor(Math.random() * 4);
        randomObject.spritesheet = itemArray[randomItem].spritesheet;
    }
}

function setItems()
{
    randomX = Math.floor(Math.random() * 1700);
    randomY = Math.floor(Math.random() * 600);
}

function collect()
{
    if(collected == false)
    {
        for(countItem=0; countItem <=3; countItem++)
        {
            if(itemArray[countItem].x + itemArray[countItem].width > player.x &&//left
            itemArray[countItem].x < player.x + player.width &&//right
            itemArray[countItem].y < player.y + player.height &&//top
            itemArray[countItem].y + itemArray[countItem].height > player.y)//bottom
            {
                itemArray[countItem].x = collectedX;
                itemArray[countItem].y = collectedY;
                collectedItem = countItem;
                collected = true;
            }
        }
    }
}

function give()
{
    if(collected == true)
    {
        if(player.x >= 750 && player.x <= 950 && player.y >= 0 && player.y <= 60)
        {
            if(itemArray[collectedItem].spritesheet == randomObject.spritesheet)
            {
                countScore++;
                updateLevel();
                time_current=time_current+3;
                updateTime();
                showItem();
            }
            if(itemArray[collectedItem].spritesheet != randomObject.spritesheet)
            {
                time_current--;
                updateTime();
                showItem();
            }

            itemArray[collectedItem].x = Math.floor((Math.random() * 500));
            itemArray[collectedItem].y = Math.floor(100+(Math.random() * 500));
            
            collected = false;
        }
    }
}

function updateLevel()
{
    if (countScore >=1 && levelCount==1)
    {
        levelCount++;
        countScore = 0;
        localStorage.setItem("level", levelCount);
    }
    if (countScore >=2 && levelCount==2)
    {
        levelCount++;
        countScore = 0;
        localStorage.setItem("level", levelCount);
    }
}

function updateTime()
{
    context.fillStyle = "#ff1e00";
    var fillCurrent = Math.min(Math.max(time_current/time_max), 1);
    context.fillRect(time_x, time_y, fillCurrent * time_width, time_height);
    localStorage.setItem("time", time_current);
}

function time()
{
    //background
    context.fillStyle ="#000000";
    context.fillRect(time_x, time_y, time_width, time_height);

    //filling the time
    context.fillStyle = "#ff1e00";
    var fillCurrent = Math.min(Math.max(time_current/time_max), 1);
    context.fillRect(time_x, time_y, fillCurrent * time_width, time_height);
}

function timeDec()
{
    if(time_current > 0)
    {
        count--;
        if (count == 0)
        {
            time_current = time_current - 5;
            count = 300;
        }
    }
}

//check if the player is pressing the screen to use the joystick
dynamic.on('start', function(evt, nipple)
{
    nipple.on('dir:up', function (evt, data)//up
    {
        input = new playerInput("up");
    });
    nipple.on('dir:down', function (evt, data)//down
    {
        input = new playerInput("down");
    });
    nipple.on('dir:left', function (evt, data)//left
    {
        input = new playerInput("left");
    });
    nipple.on('dir:right', function (evt, data)//right
    {
        input = new playerInput("right");
    });
    nipple.on('end', function (evt, data)//none
    {
        //console.log("mvmt stopped");
        input = new playerInput("none");
    });
 });

function animate()
{
    if(input.action != "none"){
        frameCount++;
        if(frameCount >= frameLast)
        {
            frameCount =0;
            currentLoop++;
            if(currentLoop >= walkLoop.length)
            {
                currentLoop =0;
            }
        }
    }
    else
    {
        currentLoop =0;
    }
    drawFrame(player.spritesheet, walkLoop[currentLoop], direction, player.x, player.y);
}

function drawFrame(sprite, frameX, frameY, CanvasX, canvasY)
{
    context.drawImage(sprite, frameX*width, frameY*height, width, height, CanvasX, canvasY, scaledHeight, scaledWidth)
}

//update what is happening on the screen
function update()
{
    edgeDetection();
    playerInteracton();
    timeDec();
}

function drawItems()
{
    context.drawImage(randomObject.spritesheet, randomObject.x, randomObject.y, randomObject.width, randomObject.height);
    if(levelCount == 1)
    {
        context.drawImage(ball.spritesheet, ball.x, ball.y, ball.width, ball.height);
        context.drawImage(ball2.spritesheet, ball2.x, ball2.y, ball2.width, ball2.height);
    }
    else if (levelCount == 2)
    {
        context.drawImage(ball.spritesheet, ball.x, ball.y, ball.width, ball.height);
        context.drawImage(ball2.spritesheet, ball2.x, ball2.y, ball2.width, ball2.height);
        context.drawImage(cap.spritesheet, cap.x, cap.y, cap.width, cap.height);
    }
    else if (levelCount == 3)
    {
        context.drawImage(ball.spritesheet, ball.x, ball.y, ball.width, ball.height);
        context.drawImage(ball2.spritesheet, ball2.x, ball2.y, ball2.width, ball2.height);
        context.drawImage(cap.spritesheet, cap.x, cap.y, cap.width, cap.height);
        context.drawImage(crayonGreen.spritesheet, crayonGreen.x, crayonGreen.y, crayonGreen.width, crayonGreen.height);
    }
}

//draws everything that is to be shown on the screen
function draws()
{
    //clears the canvas from any previous drawings
    context.clearRect(0, 0, canvas.width, canvas.height);
    drawItems();
    time();
    animate();
}
//loops as long as the page is open
function gameLoop()
{
    update();
    draws();
    window.requestAnimationFrame(gameLoop);
}
window.requestAnimationFrame(gameLoop);